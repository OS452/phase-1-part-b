/**
 * phase1.c
 * Part B
 *
 * @author: Hercy (Yunhao Zhang)
 * @author: David Carrig
 *
 */

#include <stddef.h>
#include "usloss.h"
#include "phase1.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/* -------------------------- Globals ------------------------------------- */
#define DEBUG_FLAG      0 // Debug flag - display more detail info
#define INFO_FLAG       0 // Display simple info

#define MAX_PRIORITY    6

#define UNUSED         -100

#define INVALID        -1
#define RUNNING         0
#define READY           1
#define KILLED          2
#define QUIT            3
#define WAITING         4


USLOSS_Context prev;


/* Linked list */
typedef struct processNode
{
    int PID;
    struct processNode *next;
} processNode;
typedef struct semNode
{
    P1_Semaphore s;
    struct semNode *next;
} semNode;

processNode *readyList;
processNode *blockedList;



/* Semaphore */
typedef struct Semaphore
{
    int count;
    processNode * q;
} Semaphore;

int totalSemaphore;

/* Process control block */
typedef struct PCB
{
    char name[20];
    int PID;
    int parentPID;
    int priority;
    int numOfChildren;
    int prevStartTime;
    int CPUTime;
    USLOSS_Context context;
    int state;
    int status;
    void *stack;
    int stackSize;
    int (*startFunc)(void *);   /* Starting function */
    void *startArg;             /* Arg to starting function */
    processNode * children;
	Semaphore * semaphore;
    semNode * joinList;
    
} PCB;


/* The process table */
PCB procTable[P1_MAXPROC];

/* Super useful index i & j */
int i, j;

/* Current process ID */
int pid = -1;

/* Number of processes */
int numProcs = 0;



/* Linked list operations */
void insertFront(processNode **list, int newPID);
void insertRear(processNode **list, int newPID);
void insertRearSem(semNode **list, P1_Semaphore newSem);
int delete(processNode **list, int somePID);
void printLinkedList(processNode *list);

/* Ready list operations */
int addToReadyList(int newPID);

/* Interrupt operations */
void int_enable();
void int_disable();


/* Interrupt handlers */
int clockStatus, alarmNum;
USLOSS_DeviceRequest diskRequest[2];
int terminalStatus[4];
int clockTick;
P1_Semaphore clockSemaphore, alarmSemaphore;
P1_Semaphore disks[2];
P1_Semaphore terminals[4];

void clock_handler(int type, void *offset);
void alarm_handler(int type, void *offset);
void disk_handler(int type, void *offset);
void term_handler(int type, void *offset);
void syscall_handler(int type, void *offset);



static int sentinel(void *arg);
static void launch(void);
void checkKernelMode();

/* -------------------------- Functions ----------------------------------- */



/* ------------------------------------------------------------------------
 Name - P1_GetPID
 Purpose - Return the PID of the currently running process
 ----------------------------------------------------------------------- */
int P1_GetPID()
{
    return pid;
}

/* ------------------------------------------------------------------------
 Name - P1_GetState
 Returns:   -1: invalid PID
 0: the process is running
 1: the process is ready
 2: the process has been killed
 3: the process has quit
 4: the process is waiting on a semaphore
 ----------------------------------------------------------------------- */
int P1_GetState(int PID)
{
    if(PID < 0 || PID > P1_MAXPROC - 1)
    {
        return INVALID;
    }
    return procTable[PID].state;
}

/* ------------------------------------------------------------------------
 Name - P1_DumpProcesses
 Purpose - Print process information list
 ----------------------------------------------------------------------- */
void P1_DumpProcesses()
{
    
    if(DEBUG_FLAG)
    {
        USLOSS_Console("*** @P1_DumpProcesses(): Start printing process table...\n");
    }
    PCB pcb;
    
    for(i = 0; i < P1_MAXPROC; i++)
    {
        pcb = procTable[i];
        if(pcb.PID >= 0)
        {
            USLOSS_Console("     PID: %d, Parent's PID: %d, Priority: %d, State: %d, Children: %d, CPU Time Consumed: %d - %s\n",
                           pcb.PID,
                           pcb.parentPID,
                           pcb.priority,
                           pcb.state,
                           pcb.numOfChildren,
                           pcb.CPUTime,
                           pcb.name);
        }
    }
    if(DEBUG_FLAG)
    {
        USLOSS_Console("     Print process table FINISHED\n");
    }
    
}


/* ------------------------------------------------------------------------
 Name - dispatcher
 Purpose - runs the highest priority runnable process
 Parameters - none
 Returns - nothing
 Side Effects - runs a process
 ----------------------------------------------------------------------- */
void dispatcher()
{
    /*
     * Run the highest priority runnable process. There is guaranteed to be one
     * because the sentinel is always runnable.
     */
    
    if(DEBUG_FLAG || INFO_FLAG)
    {
        USLOSS_Console("*** @dispatcher(): Started\n");
    }
    
    /* Check kernel mode */
    if(DEBUG_FLAG)
    {
        USLOSS_Console("*** @dispatcher(): ");
    }
    checkKernelMode();
    
    if(DEBUG_FLAG || INFO_FLAG)
    {
        USLOSS_Console("*** @dispatcher(): Printing readyList...\n");
        printLinkedList(readyList);
        USLOSS_Console("     Print readyList FINISHED\n");
		P1_DumpProcesses();
        
    }
    
    
    processNode * nextProcess = readyList;
    if(DEBUG_FLAG)
    {
        USLOSS_Console("*** @dispatcher(): Running PID: %d (%d) - %s...\n",
                       nextProcess->PID,
                       procTable[nextProcess->PID].priority,
                       procTable[nextProcess->PID].name);
    }
    if(DEBUG_FLAG)
    {
        USLOSS_Console("*** @dispatcher(): Current CPU clock time: %d\n", USLOSS_Clock());
    }
    
    if(procTable[pid].state == KILLED)
    {
        P1_Quit(procTable[pid].status);
    }
    // Check sentinel
    else if(pid == 0)
    {
        pid = nextProcess->PID;
        
        // CPU time
        procTable[pid].CPUTime += USLOSS_Clock() - procTable[pid].prevStartTime;
        procTable[pid].prevStartTime = USLOSS_Clock();
        USLOSS_ContextSwitch(NULL, &procTable[pid].context);
    }
    // Not sentinel
    else
    {
        int prevPID = pid;
        pid = nextProcess->PID;
        
        // Check killed
        if(procTable[pid].state == KILLED)
        {
            P1_Quit(KILLED);
        }
        
        // CPU time
        procTable[pid].prevStartTime = USLOSS_Clock();
        procTable[prevPID].CPUTime += USLOSS_Clock() - procTable[prevPID].prevStartTime;
        
        if(pid != prevPID)
        {
            USLOSS_ContextSwitch(&procTable[prevPID].context, &procTable[pid].context);
        }
        else
        {
            //USLOSS_ContextSwitch(NULL, &procTable[pid].context);
            //USLOSS_Console(">>>> ??? - %d, should NOT be seen this!\n", pid);
        }
        
    }
}

int P1_ReadTime()
{
    return procTable[pid].CPUTime;
}


void checkKernelMode()
{
    if(DEBUG_FLAG)
    {
        USLOSS_Console("checkKernelMode(): Checking kernel mode...");
    }
    if((USLOSS_PsrGet() & 0x1) != 1)
    {
        USLOSS_Console("ERROR!\n");
        USLOSS_Console("     NOT in kernel mode!\n");
        USLOSS_Console("     Quting...\n");
        P1_Quit(1);
    }
    if(DEBUG_FLAG)
    {
        USLOSS_Console(" OK\n");
    }
}


/* ------------------------------------------------------------------------
 Name - startup
 Purpose - Initializes semaphores, process lists and interrupt vector.
 Start up sentinel process and the P2_Startup process.
 Parameters - none, called by USLOSS
 Returns - nothing
 Side Effects - lots, starts the whole thing
 ----------------------------------------------------------------------- */
void startup()
{
    if(DEBUG_FLAG || INFO_FLAG)
    {
        USLOSS_Console("*** @startup(): Started\n");
    }
    
    /* Check kernel mode */
    if(DEBUG_FLAG)
    {
        USLOSS_Console("*** @startup(): Checking kernel mode...");
    }
    if((USLOSS_PsrGet() & 0x1) != 1)
    {
        USLOSS_Console("ERROR!\n");
        USLOSS_Console("     NOT in kernel mode!\n");
        USLOSS_Console("     Halting...\n");
        USLOSS_Halt(1);
    }
    if(DEBUG_FLAG)
    {
        USLOSS_Console(" OK\n");
    }
    
    
    /* Initialize the process table */
    if(DEBUG_FLAG)
    {
        USLOSS_Console("*** @startup(): Initializing the process table...");
    }
    for(i = 0; i < P1_MAXPROC; i++)
    {
        procTable[i].PID = -1;
        procTable[i].state = UNUSED;
    }
    if(DEBUG_FLAG)
    {
        USLOSS_Console(" OK\n");
    }
    
    if(DEBUG_FLAG && pid == 0)
    {
        USLOSS_Console(" OK\n");
    }
    
    /* Initialize the Ready list, Blocked list, etc. */
    if(DEBUG_FLAG)
    {
        USLOSS_Console("*** @startup(): Initializing the Ready list, Blocked list...");
    }
    readyList = NULL;
    blockedList = NULL;
    if(DEBUG_FLAG)
    {
        USLOSS_Console(" OK\n");
    }
    
    /* Initialize the interrupts */
    if(DEBUG_FLAG)
    {
        USLOSS_Console("*** @startup(): Initializing the Interrupt Vector...");
    }
    clockTick = 0;
    USLOSS_IntVec[USLOSS_CLOCK_INT]     = clock_handler; //clock_handler;
    USLOSS_IntVec[USLOSS_CLOCK_DEV]     = clock_handler; //clock_handler;
    
    USLOSS_IntVec[USLOSS_ALARM_INT]     = alarm_handler; //alarm_handler;
    USLOSS_IntVec[USLOSS_ALARM_DEV]     = alarm_handler; //alarm_handler;
    
    USLOSS_IntVec[USLOSS_TERM_INT]      = term_handler; //term_handler;
    USLOSS_IntVec[USLOSS_TERM_DEV]      = term_handler; //term_handler;
    
    USLOSS_IntVec[USLOSS_SYSCALL_INT]   = syscall_handler; //syscall_handler;
    
    USLOSS_IntVec[USLOSS_DISK_INT]      = disk_handler; //disk_handler;
    USLOSS_IntVec[USLOSS_DISK_DEV]      = disk_handler; //disk_handler;
    
    //USLOSS_IntVec[USLOSS_MMU_INT]       = NULL; //mmu_handler;
    
    if(DEBUG_FLAG)
    {
        USLOSS_Console(" SKIPPED\n");
    }
    
    /* Initialize the semaphores here */
    if(DEBUG_FLAG)
    {
        USLOSS_Console("*** @startup(): Initializing the semaphores...");
    }
    clockSemaphore = P1_SemCreate(0);
    alarmSemaphore = P1_SemCreate(0);
    for(i = 0; i < 2; i++)
    {
        disks[i] = P1_SemCreate(0);
    }
    for(i = 0; i < 4; i++)
    {
        terminals[i] = P1_SemCreate(0);
    }
    totalSemaphore = 0;
    if(DEBUG_FLAG)
    {
        USLOSS_Console("*** @startup(): Semaphores initialized\n\n");
    }
    
    
    /* Startup a sentinel process */
    /* HINT: you don't want any forked processes to run until startup is finished.
     * You'll need to do something in your dispatcher to prevent that from happening.
     * Otherwise your sentinel will start running right now and it will call P1_Halt.
     */
    P1_Fork("sentinel", sentinel, NULL, USLOSS_MIN_STACK, 6);
    
    /* Start the P2_Startup process */
    P1_Fork("P2_Startup", P2_Startup, NULL, 4 * USLOSS_MIN_STACK, 1);
    
    dispatcher();
    
    /* Should never get here (sentinel will call USLOSS_Halt) */
    
    return;
} /* End of startup */


/* ------------------------------------------------------------------------
 Name - finish
 Purpose - Required by USLOSS
 Parameters - none
 Returns - nothing
 Side Effects - none
 ----------------------------------------------------------------------- */
void finish()
{
    USLOSS_Console("Goodbye.\n");
} /* End of finish */

int findEmptyPID()
{
    int emptyPID = -1;
    
    int i;
    for(i = 0; i < P1_MAXPROC; i++)
    {
        if(procTable[i].PID == -1 && procTable[i].state == UNUSED)
        {
            return i;
        }
    }
    
    if(emptyPID == -1)
    {
        USLOSS_Console("\n*** @findEmptyPID(): Unable to find a empty slot.\n");
        USLOSS_Halt(1);
    }
    
    return emptyPID;
}

/* ------------------------------------------------------------------------
 Name - P1_Fork
 Purpose - Gets a new process from the process table and initializes
 information of the process.  Updates information in the
 parent process to reflect this child process creation.
 Parameters - the process procedure address, the size of the stack and
 the priority to be assigned to the child process.
 Returns - the process id of the created child or an error code.
 Side Effects - ReadyList is changed, procTable is changed, Current
 process information changed
 ------------------------------------------------------------------------ */
int P1_Fork(char *name, int (*f)(void *), void *arg, int stacksize, int priority)
{
    int_disable(); // Disable interrupts
    
    if(DEBUG_FLAG)
    {
        USLOSS_Console("*** @P1_Fork(): Started\n");
    }
    
    /* Check kernel mode */
    if(DEBUG_FLAG)
    {
        USLOSS_Console("*** @P1_Fork(): ");
    }
    checkKernelMode();
    
    /* Check priority */
    if(DEBUG_FLAG)
    {
        USLOSS_Console("*** @P1_Fork(): Checking priority... ");
    }
    if(priority <= 6 && priority >= 1)
    {
        if(DEBUG_FLAG)
        {
            USLOSS_Console("OK\n");
        }
    }
    else
    {
        USLOSS_Console("Invalid priotity!!!\n");
        return -1;
    }
    
    
    
    if(DEBUG_FLAG || INFO_FLAG)
    {
        USLOSS_Console("*** @P1_Fork(): Initializing current process: %s...", name);
    }
    
    /* newPid = pid of empty PCB here */
    int newPid = findEmptyPID();
    
    void *stack = NULL;
    stack = malloc(stacksize);
    strcpy(procTable[newPid].name, name);

    procTable[newPid].PID = newPid;
    procTable[newPid].parentPID = pid;
    procTable[newPid].priority = priority;
    procTable[newPid].numOfChildren = 0;
    procTable[newPid].stackSize = stacksize;
    procTable[newPid].state = READY;
    procTable[newPid].stack = stack;
    procTable[newPid].status = 0;
	procTable[newPid].semaphore = P1_SemCreate(0);
    
    // Clock initialized
    procTable[pid].prevStartTime = USLOSS_Clock();
    procTable[newPid].CPUTime = 0;
    
    procTable[newPid].startFunc = f;
    procTable[newPid].startArg = arg;
    
    procTable[newPid].children = NULL;
    
    // Add to ready list
    addToReadyList(newPid);
    
    USLOSS_ContextInit(&(procTable[newPid].context), USLOSS_PsrGet(), stack, stacksize, launch);
    
    numProcs++;
    
    
    if(DEBUG_FLAG || INFO_FLAG)
    {
        USLOSS_Console(" OK\n");
    }
    
    // Set parent info
    if(pid > -1)
    {
        procTable[pid].numOfChildren++;
        insertRear(&procTable[pid].children, newPid);
        //USLOSS_Console("~~~~~~~~~~~~~~~~~~~~~~~~~\n");
        //printLinkedList(procTable[pid].children);
        //USLOSS_Console("~~~~~~~~~~~~~~~~~~~~~~~~~\n");
        //procTable[pid].state = READY;
    }

    // Run dispatcher if not sentinel
    if(newPid)
    {
        int_enable(); // Enable interrupts
        dispatcher();
    }
    
    int_enable(); // Enable interrupts
    return newPid;
} /* End of fork */


/* ------------------------------------------------------------------------
 Name - launch
 Purpose - Dummy function to enable interrupts and launch a given process
 upon startup.
 Parameters - none
 Returns - nothing
 Side Effects - enable interrupts
 ------------------------------------------------------------------------ */
void launch(void)
{
    /* Check kernel mode */
    if(DEBUG_FLAG)
    {
        USLOSS_Console("*** @launch(): ");
    }
    checkKernelMode();
    
    int rc;
    USLOSS_PsrSet(USLOSS_PsrGet() | USLOSS_PSR_CURRENT_INT);
    
    rc = procTable[pid].startFunc(procTable[pid].startArg);
    /* quit if we ever come back */
    P1_Quit(rc);
} /* End of launch */

/* ------------------------------------------------------------------------
 Name - P1_Quit
 Purpose - Causes the process to quit and wait for its parent to call P1_Join.
 Parameters - quit status
 Returns - nothing
 Side Effects - the currently running process quits
 ------------------------------------------------------------------------ */
void P1_Quit(int status)
{
    int_disable(); // Disable interrupts
    
    /* Check kernel mode */
    if(DEBUG_FLAG)
    {
        USLOSS_Console("*** @P1_Quit(): ");
    }
    checkKernelMode();
    

    if(procTable[pid].status != 0)
    {
        status = procTable[pid].status;
    }
    int tmpPID = pid;
    switch(status)
    {
        // Halt with problem
        case 1:
            if(DEBUG_FLAG || INFO_FLAG)
            {
                USLOSS_Console("*** @P1_Quit(): Halting (PID: %d) with a problem...\n", pid);
            }
            USLOSS_Halt(1);
            break;
        
        // Halt with no problem
            /*
        case 0:
            if(DEBUG_FLAG)
            {
                USLOSS_Console("*** @P1_Quit(): Halting (PID: %d) with no problem...\n", pid);
            }
            USLOSS_Halt(0);
            break;
            */
        default:
            break;
    }
    
    if(procTable[pid].state == UNUSED)// || procTable[pid].state == QUIT)
    {
        USLOSS_Console("Unable to quit, this prcoess has already quit.\n");
    }
    else
    {
        
        // I'm child, I quitted.
        // Update parent infos
        for(i = 0; i < P1_MAXPROC; i++)
        {
            int x = delete(&procTable[i].children, tmpPID);
            if(x == tmpPID)
            {
                procTable[i].numOfChildren--;
                
                // Return status to its parent
                procTable[i].status = status;
            }
        }
        
        // I'm parent, I quitted.
        for(i = 0; i < P1_MAXPROC; i++)
        {
            if(procTable[i].parentPID == pid)
            {
                procTable[i].parentPID = -1;
            }
        }
        
        
        
        // Remove it from ready list
        int x = delete(&readyList, tmpPID);
        
        numProcs--;
        
        if(DEBUG_FLAG || INFO_FLAG)
        {
            USLOSS_Console("*** @P1_Quit(): Quitting: %s...\n", procTable[tmpPID].name);
        }
        if(x == -1)
        {
            USLOSS_Console("Unable to delete %d from the ready list.\n", pid);
        }
        
        
        // Set current pid into parent's pid
        //pid = procTable[tmpPID].parentPID;
        
		// Calls P1_V on each join semaphore of quitting process
		semNode * tmp = procTable[tmpPID].joinList;
		while(tmp)
		{
			P1_Semaphore sem = tmp->s;
			P1_V(sem);
			tmp = tmp->next;
		}

        // Remove it from process table
        procTable[tmpPID].state = QUIT;
        procTable[tmpPID].status = 0;
        //procTable[tmpPID].PID = -1;
        //procTable[tmpPID].state = UNUSED;

        if(DEBUG_FLAG)
        {
            USLOSS_Console("*** @P1_Quit(): OK\n");
        }
    }
    
    // Switch to protection process sentinel
    pid = 0;
    
    int_enable(); // Enable interrupts
    
    // Resume join
    /*
    if(procTable[tmpPID].semaphore->q)
    {
        USLOSS_Console("Restoring semaphore...\n");
        //pid = procTable[tmpPID].semaphore->q->PID;
        P1_V(procTable[tmpPID].semaphore);
    }
    */
    if(status != 100)
    {
        dispatcher();
    }
    //USLOSS_ContextSwitch(NULL, &prev);
}

/* ------------------------------------------------------------------------
 P1_Kill()
 Returns:  -2: PID belongs to the current process
 -1: invalid PID
 0: success
 ------------------------------------------------------------------------ */
int P1_Kill(int PID)
{
    int_disable(); // Disable interrupts
    
    /* Check kernel mode */
    if(DEBUG_FLAG)
    {
        USLOSS_Console("*** @P1_Kill(): ");
    }
    checkKernelMode();
    
    if(DEBUG_FLAG || INFO_FLAG)
    {
        USLOSS_Console("*** @P1_Kill(): Killing %s (%d)...\n", procTable[PID].name, PID);
    }
    // Invalid PID, sentinal shall NOT be killed.
    if(procTable[PID].state == UNUSED || PID < 1 || PID > P1_MAXPROC - 1 || procTable[PID].state == QUIT || procTable[PID].state == KILLED)
    {
        if(DEBUG_FLAG || INFO_FLAG)
        {
            USLOSS_Console("*** @P1_Kill(): INVALID\n");
        }
        int_enable(); // Enable interrupts
        return INVALID;
    }
    // PID belongs to the current process
    if(PID == pid)
    {
        if(DEBUG_FLAG || INFO_FLAG)
        {
            USLOSS_Console("*** @P1_Kill(): Can't kill yourself\n");
        }
        int_enable(); // Enable interrupts
        return -2;
    }
    
    procTable[PID].state = KILLED;
    
    int_enable(); // Enable interrupts
    return 0;
}

/* ------------------------------------------------------------------------
 Name - P1_Join
 Returns:   -1: the process doesn�t have any children
          >= 0: the PID of the child that quit
 ------------------------------------------------------------------------ */
int P1_Join(int *status)
{
    /* Check kernel mode */
    if(DEBUG_FLAG)
    {
        USLOSS_Console("*** @P1_Join(): ");
    }
    checkKernelMode();

	if(procTable[pid].numOfChildren == 0)
		return -1;

	P1_Semaphore joinSem = P1_SemCreate(0);

	processNode * tmp = procTable[pid].children;

	while(tmp)
	{
		int cPID = tmp->PID;
		insertRearSem(&procTable[cPID].joinList, joinSem);
		tmp = tmp->next;
	}
    
	if(DEBUG_FLAG || INFO_FLAG)
    {
        USLOSS_Console("*** @P1_Join(): Process %s waiting for child to quit\n", procTable[pid].name);
    }
	
    
        
	P1_P(joinSem);
	
	if(DEBUG_FLAG || INFO_FLAG)
    {
		USLOSS_Console("*** @P1_Join(): Process %s done waiting for child to quit\n", procTable[pid].name);
	}

	return 0;
	

}


/* =========================================================================== */
/* @START - Semaphore operations */
/* ------------------------------------------------------------------------
 Name - P1_SemCreate
 Returns:    Newly created semaphore with its initial value set to "value"
 ------------------------------------------------------------------------ */
P1_Semaphore P1_SemCreate(unsigned int value)
{
    int_disable(); // Disable interrupts
    
    /* Check kernel mode */
    if(DEBUG_FLAG)
    {
        USLOSS_Console("*** @P1_SemCreate(): ");
    }
    checkKernelMode();

    
    /* Create a semaphore */
    totalSemaphore++;
    
    if(totalSemaphore > P1_MAXSEM)
    {
        USLOSS_Console("Max amount of semaphores %d reached\n", P1_MAXSEM);
        USLOSS_Console("Halting...\n");
        //P1_Quit(1);
        USLOSS_Halt(1);
    }
    
    Semaphore * newSem;
    newSem = (Semaphore *)malloc(sizeof(Semaphore));
    
    newSem->count = value;
    newSem->q = NULL;
 
    int_enable(); // Enable interrupts
    
    return (P1_Semaphore)newSem;
}

/* ------------------------------------------------------------------------
 Name - P1_SemFree
 Returns:  -1: the semaphore is invalid
            0: success
 ------------------------------------------------------------------------ */
int P1_SemFree(P1_Semaphore sem)
{
    int_disable(); // Disable interrupts
    
    /* Check kernel mode */
    if(DEBUG_FLAG)
    {
        USLOSS_Console("*** @P1_SemCreate(): ");
    }
    checkKernelMode();
    
    //Semaphore * tmp = (Semaphore *) sem;
    if(sem)
    {
        free(sem);
        totalSemaphore--;
        int_enable(); // Enable interrupts
        return 0;
    }
    
    
    int_enable(); // Enable interrupts
    
    return -1;
}

/* ------------------------------------------------------------------------
 P1_P()
 Returns:  -2: The process was killed
           -1: The semaphore is invalid
            0: success
 ------------------------------------------------------------------------ */
int P1_P(P1_Semaphore sem)
{
    //Semaphore * tmp = (Semaphore *) sem;
    while(1)
    {
        int_disable(); // Disable interrupts
        
        // The process was killed
        if(procTable[pid].state == KILLED)
        {
            int_enable(); // Enable interrupts
            return -2;
        }
        
        if(((Semaphore *) sem)->count > 0)
        {
            // Decrease count
            ((Semaphore *) sem)->count--;
            goto done;
        }
        
        // Add to queue
        insertRear(&((Semaphore *) sem)->q, pid);
        procTable[pid].state = WAITING;
        // Remove from readyList
        delete(&readyList, pid);
        
        int_enable();
        dispatcher();
    }
    return INVALID;
    
done:
    int_enable(); // Enable interrupts
    
    return 0;
}


/* ------------------------------------------------------------------------
 P1_V()
 Returns:  -1: The semaphore is invalid
            0: success
 ------------------------------------------------------------------------ */
int P1_V(P1_Semaphore sem)
{
    int_disable(); // Disable interrupts
    
    // Invalid
    if(((Semaphore *) sem)->q == NULL)
    {
        return INVALID;
    }
    
    int aPorcess = ((Semaphore *) sem)->q->PID;
    
    // Remove from queue
    delete(&((Semaphore *) sem)->q, aPorcess);
    
    procTable[aPorcess].state = RUNNING;
    // Add to readyList
    addToReadyList(aPorcess);
    
    // Increase count
    ((Semaphore *) sem)->count++;
    

    int_enable(); // Enable interrupts
    
    return 0;
}


/* @END - Semaphore operations */
/* =========================================================================== */







/* ------------------------------------------------------------------------
 Name - sentinel
 Purpose - The purpose of the sentinel routine is two-fold.  One
 responsibility is to keep the system going when all other
 processes are blocked.  The other is to detect and report
 simple deadlock states.
 Parameters - none
 Returns - nothing
 Side Effects -  if system is in deadlock, print appropriate error
 and halt.
 ----------------------------------------------------------------------- */
int sentinel(void *notused)
{
    /*
    while(numProcs > 1)
    {
        // Check for deadlock here
        USLOSS_WaitInt();
		USLOSS_Console("*** System Deadlocked\n");
		USLOSS_Halt(1);
    }*/
    
    int processWaiting = 0;
    int processRunning = 0;
    
    for(i = 1; i < P1_MAXPROC; i++)
    {
        if(procTable[i].state == RUNNING || procTable[i].state == READY || procTable[i].state == KILLED)
        {
            processRunning++;
        }
        if(procTable[i].state == WAITING)
        {
            processWaiting++;
        }
    }
    
    // Check for deadlock here
    if(processWaiting >= 1 && processRunning == 0)
    {
        USLOSS_WaitInt();
        USLOSS_Console("*** @sentinel(): System Deadlocked\n");
        USLOSS_Halt(1);
    }
    
    if(DEBUG_FLAG || INFO_FLAG)
    {
        USLOSS_Console("*** @sentinel(): No more process running\n");
        USLOSS_Console("*** @sentinel(): Halting...\n");
    }
    //P1_Quit(0);
    USLOSS_Halt(0);
    /* Never gets here. */
    return 0;
} /* End of sentinel */










/* =========================================================================== */
/* @START - Interrupt operations */

void int_enable()
{
    if(DEBUG_FLAG)
    {
        USLOSS_Console("*** @int_enable(): ");
    }
    checkKernelMode();
    
    if(DEBUG_FLAG)
    {
        USLOSS_Console("*** @int_enable(): Enabling interrupts...");
    }
    USLOSS_PsrSet(USLOSS_PsrGet() | 0x02);
    if(DEBUG_FLAG)
    {
        USLOSS_Console(" OK\n");
    }
}

void int_disable()
{
    if(DEBUG_FLAG)
    {
        USLOSS_Console("*** @int_enable(): ");
    }
    checkKernelMode();
    
    if(DEBUG_FLAG)
    {
        USLOSS_Console("*** @int_enable(): Enabling interrupts...");
    }
    USLOSS_PsrSet(USLOSS_PsrGet() & 0xFD);
    if(DEBUG_FLAG)
    {
        USLOSS_Console(" OK\n");
    }
}
/* ------------------------------------------------------------------------
 P1_WaitDevice(int type, int unit, int *status)
 Returns:  -3: the process was killed
           -2: invalid type
           -1: invalid unit
            0: success
 ------------------------------------------------------------------------ */

int P1_WaitDevice(int type, int unit, int *status)
{
    if(DEBUG_FLAG || INFO_FLAG)
    {
        USLOSS_Console("*** @P1_WaitDevice(): Called\n");
    }
    checkKernelMode();
    
    switch(type)
    {
        case USLOSS_CLOCK_DEV:
            if(unit == 0)
            {
                P1_P(clockSemaphore);
                status = &clockStatus;
            }
            else
            {
                return INVALID;
            }
            break;
            
        case USLOSS_ALARM_DEV:
            if(unit == 0)
            {
                P1_P(alarmSemaphore);
                status = &alarmNum;
            }
            else
            {
                return INVALID;
            }
            break;
            
        case USLOSS_TERM_DEV:
            if(unit >= 0 && unit <= 3)
            {
                P1_P(terminals[unit]);
                status = &terminalStatus[unit];
            }
            else
            {
                return INVALID;
            }
            break;
            
        case USLOSS_DISK_DEV:
            if(unit >= 0 && unit <= 1)
            {
                P1_P(disks[unit]);
                status = &diskRequest[unit].opr;
            }
            else
            {
                return INVALID;
            }
            break;
            
        default:
            USLOSS_Console("*** @P1_WaitDevice(): Invalid type\n");
            return -2;
            break;
    }
    
    return 0;
}




/* ------------------------------------------------------------------------
 Name - clock_handler
 20 ms on Lectura (1/50 = 0.02 s)
 Every 100ms
 ----------------------------------------------------------------------- */
void clock_handler(int type, void *offset)
{
    if(DEBUG_FLAG)
    {
        USLOSS_Console("*** @clock_handler(): clock_handler started with %d\n", type);
    }
    clockTick++;
    
    int timeUnits = 100 / USLOSS_CLOCK_MS;
    if(clockTick != 0 && clockTick % timeUnits == 0)
    {
        int x = USLOSS_DeviceInput(USLOSS_CLOCK_DEV, 0, &clockStatus);
        if(x == USLOSS_DEV_READY)
        {
            USLOSS_Console("clock_handler called\n");
            P1_V(clockSemaphore);
        }
        else if(x == USLOSS_DEV_BUSY)
        {
            USLOSS_Console("*** @clock_handler(): clock_handler is busy\n");
        }
        else if(x == USLOSS_DEV_ERROR)
        {
            USLOSS_Console("*** @clock_handler(): clock_handler error\n");
        }
        else
        {
            USLOSS_Console("*** @clock_handler(): clock_handler (%d) should not be seeing this\n", x);
        }
    }
}

/* ------------------------------------------------------------------------
 Name - alarm_handler
 ----------------------------------------------------------------------- */
void alarm_handler(int type, void *offset)
{
    if(DEBUG_FLAG)
    {
        USLOSS_Console("*** @alarm_handler(): alarm_handler started with %d\n", type);
    }
    int x = USLOSS_DeviceOutput(USLOSS_ALARM_DEV, 0, (void *)&alarmNum);
    if(x == USLOSS_DEV_OK)
    {
        USLOSS_Console("alarm_handler called\n");
        P1_V(alarmSemaphore);
    }
    else if(x == USLOSS_DEV_INVALID)
    {
        USLOSS_Console("*** @alarm_handler(): alarm_handler invalid\n");
    }
    else
    {
        USLOSS_Console("*** @alarm_handler(): alarm_handler (%d) should not be seeing this\n", x);
    }
}

/* ------------------------------------------------------------------------
 Name - disk_handler
 ----------------------------------------------------------------------- */
void disk_handler(int type, void *offset)
{
    if(DEBUG_FLAG)
    {
        USLOSS_Console("*** @disk_handler(): disk_handler started with %d\n", type);
    }
    USLOSS_DeviceRequest request;
    int unit = (int) offset;
    int x = USLOSS_DeviceOutput(USLOSS_DISK_DEV, unit, &request);
    if(x == USLOSS_DEV_OK)
    {
        if(unit >= 0 && unit <= 1)
        {
            USLOSS_Console("disk_handler called\n");
            diskRequest[unit] = request;
            P1_V(disks[unit]);
        }
        else
        {
            USLOSS_Console("*** @disk_handler(): Unit invalid\n");
        }
        
    }
    else if(x == USLOSS_DEV_INVALID)
    {
        USLOSS_Console("*** @disk_handler(): disk_handler invalid\n");
    }
    else
    {
        USLOSS_Console("*** @disk_handler(): disk_handler (%d) should not be seeing this\n", x);
    }

}

/* ------------------------------------------------------------------------
 Name - term_handler
 ----------------------------------------------------------------------- */
void term_handler(int type, void *offset)
{
    if(DEBUG_FLAG)
    {
        USLOSS_Console("*** @term_handler(): term_handler started with %d\n", type);
    }
    int status, unit = (int) offset;
    int x = USLOSS_DeviceInput(USLOSS_TERM_DEV, unit, &status);
    if(x == USLOSS_DEV_READY)
    {
        if(unit >= 0 && unit <= 3)
        {
            USLOSS_Console("term_handler called\n");

            terminalStatus[unit] = status;
            P1_V(terminals[unit]);
        }
        else
        {
            USLOSS_Console("*** @term_handler(): Unit invalid\n");
        }
        
    }
    else if(x == USLOSS_DEV_BUSY)
    {
        USLOSS_Console("*** @term_handler(): term_handler busy\n");
    }
    else if(x == USLOSS_DEV_ERROR)
    {
        USLOSS_Console("*** @term_handler(): term_handler error\n");
    }
    else
    {
        USLOSS_Console("*** @term_handler(): term_handler (%d) should not be seeing this\n", x);
    }

}

/* ------------------------------------------------------------------------
 Name - syscall_handler
 ----------------------------------------------------------------------- */
void syscall_handler(int type, void *offset)
{
    USLOSS_Console("System call %d not implemented.", type);
    P1_Quit(1);
}

/* @END - Interrupt operations */
/* =========================================================================== */



/* =========================================================================== */
/* @START - Linked list operations */
/* ------------------------------------------------------------------------
 Name - insertFront
 Purpose - Insert element to the front of the given linked list
 ----------------------------------------------------------------------- */
void insertFront(processNode **list, int newPID)
{
    
    processNode *tmp;
    tmp = (processNode *)malloc(sizeof(processNode));
    tmp->PID = newPID;
    
    if(*list == NULL)
    {
        *list = (processNode *)malloc(sizeof(processNode));
        *list = tmp;
        (*list)->next = NULL;
    }
    else
    {
        tmp->next = *list;
        *list = tmp;
    }
    
}

/* ------------------------------------------------------------------------
 Name - insertRear
 Purpose - Insert element at the rear of the given linked list
 ----------------------------------------------------------------------- */
void insertRear(processNode **list, int newPID)
{
    processNode *aProcess = (processNode *)malloc(sizeof(processNode));
    aProcess->PID = newPID;
    aProcess->next = NULL;
    
    processNode *tmp = *list;
    
    if(*list == NULL)
    {
        *list = (processNode *)malloc(sizeof(processNode));
        *list = aProcess;
    }
    else
    {
        while(tmp->next)
        {
            tmp = tmp->next;
        }
        
        tmp->next = aProcess;
    }
}
void insertRearSem(semNode **list, P1_Semaphore newSem)
{
    semNode *aSem = (semNode *)malloc(sizeof(semNode));
    aSem->s = newSem;
    aSem->next = NULL;
    
    semNode *tmp = *list;
    
    if(*list == NULL)
    {
        *list = (semNode *)malloc(sizeof(semNode));
        *list = aSem;
    }
    else
    {
        while(tmp->next)
        {
            tmp = tmp->next;
        }
        
        tmp->next = aSem;
    }
}

/* ------------------------------------------------------------------------
 Name - delete
 Purpose - Delete element from the given linked list
 ----------------------------------------------------------------------- */
int delete(processNode **list, int somePID)
{
    processNode *tmp = *list;
    processNode *freer;
    
    if(*list == NULL)
    {
        return -1;
    }
    else if(tmp->next == NULL)
    {
        if(tmp->PID == somePID)
        {
            freer = *list;
            if(freer != NULL)
            {
                free(freer);
            }
            *list = NULL;
            return somePID;
        }
    }
    else
    {
        processNode *current;
        
        while(tmp->next)
        {
            if(tmp->PID == somePID)
            {
                freer = tmp;
                if(NULL) // TODO: I'll fix you later
                {
                    free(freer);
                }
                *tmp = *tmp->next;
                return somePID;
            }
            current = tmp;
            tmp = tmp->next;
        }
        
        if(tmp->PID == somePID)
        {
            freer = current->next;
            if(freer != NULL)
            {
                free(freer);
            }
            current->next = NULL;
            return somePID;
        }
    }
    return -1;
}

/* ------------------------------------------------------------------------
 Name - printLinkedList
 Purpose - Print the given linked list
 ----------------------------------------------------------------------- */
void printLinkedList(processNode *list)
{
    if(DEBUG_FLAG)
    {
        //USLOSS_Console("*** @printLinkedList(): Printing linked list...\n");
    }
    processNode *tmp = list;
    while(tmp)
    {
        printf("     PID: %d (%d) - %s\n", tmp->PID, procTable[tmp->PID].priority, procTable[tmp->PID].name);
        tmp = tmp->next;
    }
    if(DEBUG_FLAG)
    {
        //USLOSS_Console("     Print linked list FINISHED\n");
    }
}


/* @END - Linked list operations */
/* =========================================================================== */



/* =========================================================================== */
/* @START - Ready list operations */

/* ------------------------------------------------------------------------
 Name - addToReadyList
 Purpose - Find a correct place and insert the new process
 ----------------------------------------------------------------------- */
int addToReadyList(int newPID)
{
    int newPriority = procTable[newPID].priority;
    processNode * tmp = readyList;
    
    // Check first one in the list
    if(tmp)
    {
        int thisPriority = procTable[tmp->PID].priority;
        
        // New process has a higher priority
        if(thisPriority > newPriority)
        {
            insertFront(&readyList, newPID);
            return 0;
        }
    }
    // No process in the list
    else
    {
        insertRear(&readyList, newPID);
        return 0;
    }
    
    
    while(tmp->next)
    {
        int thisPriority = procTable[tmp->next->PID].priority;
        
        // New process has a higher priority
        if(thisPriority > newPriority)
        {
            // Adding new nodes
            processNode * newNode = (processNode *)malloc(sizeof(processNode));
            newNode->next = tmp->next;
            newNode->PID = newPID;
            tmp->next = newNode;
            
            return 0;
        }
            
        tmp = tmp->next;
    }
    
    // New process has the lowest priority
    if(tmp->next == NULL)
    {
        insertRear(&readyList, newPID);
        return 0;
    }
    return -1;
}

/* @END - Ready list operations */
/* =========================================================================== */


