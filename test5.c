#include "phase1.h"
#include <assert.h>
#include <stdio.h>

int XXX(void *arg) {
    USLOSS_Console("XXX\n");
    P1_Semaphore deadSem = P1_SemCreate(0);
    P1_P(deadSem);
    return 0;
}




int PPP(void *notused)
{
    int pid;
    int status = 0;
    
    USLOSS_Console("PPP\n");
    pid = P1_Fork("ChildXXX", XXX, NULL, USLOSS_MIN_STACK, 3);
    if (pid < 0) {
        USLOSS_Console("Unable to fork child: %d\n", pid);
        status = 1;
    }
    P1_Semaphore deadSem = P1_SemCreate(0);
    P1_P(deadSem);
    return status;
}


int P2_Startup(void *notused) 
{
    int pid;
    int status = 0;

    USLOSS_Console("P2_Startup\n");
    pid = P1_Fork("ChildPPP", PPP, NULL, USLOSS_MIN_STACK, 3);
    if (pid < 0) {
        USLOSS_Console("Unable to fork child: %d\n", pid);
        status = 1;
    }
    P1_Semaphore deadSem = P1_SemCreate(0);
    P1_P(deadSem);
    return status;
}

void setup(void) {
    // Do nothing.
}

void cleanup(void) {
    // Do nothing.
}